#!/usr/bin/env python
#
# hello_sensors.py
#

from __future__ import print_function
from BrickPi import *   #import BrickPi.py file to use BrickPi operations

BrickPiSetup()  # setup the serial port for communication

BrickPi.SensorType[PORT_1] = TYPE_SENSOR_TOUCH
BrickPi.SensorType[PORT_2] = TYPE_SENSOR_LIGHT_OFF
BrickPi.SensorType[PORT_3] = TYPE_SENSOR_RAW
BrickPi.SensorType[PORT_4] = TYPE_SENSOR_ULTRASONIC_CONT

BrickPiSetupSensors()   #Send the properties of sensors to BrickPi

while (True):    #running while loop for 3 seconds
    BrickPiUpdateValues()       # Ask BrickPi to update values for sensors/motors
    print("Sensor 1: ", BrickPi.Sensor[PORT_1])
    print("Sensor 2: ", BrickPi.Sensor[PORT_2])
    print("Sensor 3: ", BrickPi.Sensor[PORT_3])
    print("Sensor 4: ", BrickPi.Sensor[PORT_4])
    print()
    time.sleep(.25)

