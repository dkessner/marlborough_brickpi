//
// remote_control_client.pde
//


import processing.net.*; 


String server = "marlpi3.local";
final int socketPort = 12345;
Client client;


void setup()
{ 
    size(300, 100); 
    client = new Client(this, server, socketPort); 
} 


void draw() 
{
    background(0);

    if (client.ip() != null)
    {
        text("Connected: " + client.ip(), 10, 30);
    }
    else
    {
        text("Unable to connect to server.", 10, 30);
    }
} 


void keyPressed()
{
    if (client.ip() == null)
        return;

    if (keyCode == UP)
        client.write("forward");
    else if (keyCode == DOWN)
        client.write("backward");
    else if (keyCode == LEFT)
        client.write("left");
    else if (keyCode == RIGHT)
        client.write("right");
}


void keyReleased()
{
    if (client.ip() == null)
        return;

    client.write("stop");
}


