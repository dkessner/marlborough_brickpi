#!/usr/bin/env python
#
# remote_control_server.py
#


from BrickPi import *
import socket


socket_port = 12345
motor1 = PORT_A
motor2 = PORT_D
speed = 400


def forward():
	BrickPi.MotorSpeed[motor1] = speed  
	BrickPi.MotorSpeed[motor2] = speed  

def left():
	BrickPi.MotorSpeed[motor1] = -speed  
	BrickPi.MotorSpeed[motor2] = speed 

def right():
	BrickPi.MotorSpeed[motor1] = speed  
	BrickPi.MotorSpeed[motor2] = -speed

def backward():
	BrickPi.MotorSpeed[motor1] = -speed  
	BrickPi.MotorSpeed[motor2] = -speed

def stop():
	BrickPi.MotorSpeed[motor1] = 0  
	BrickPi.MotorSpeed[motor2] = 0
	

BrickPiSetup()  # setup the serial port for communication


BrickPi.MotorEnable[motor1] = 1 #Enable the Motor A
BrickPi.MotorEnable[motor2] = 1 #Enable the Motor B 
BrickPiSetupSensors()   #Send the properties of sensors to BrickPi
BrickPi.Timeout=10000	#Set timeout value for the time till which to run the motors after the last command is pressed
BrickPiSetTimeout()		#Set the timeout


host = ''
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', socket_port))
s.listen(1)
conn, addr = s.accept()
print('Connected by', addr)


while True:
    data = conn.recv(1024)
    if not data: break
    
    if data == "forward":
        forward()
    elif data == "backward":
        backward()
    elif data == "left":
        left()
    elif data == "right":
        right()
    elif data == "stop":
        stop()
    else:
        pass
        
    BrickPiUpdateValues() 	#Update the motor values

conn.close()


